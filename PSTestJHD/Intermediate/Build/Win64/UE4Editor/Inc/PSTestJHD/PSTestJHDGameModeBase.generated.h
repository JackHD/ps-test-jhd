// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PSTESTJHD_PSTestJHDGameModeBase_generated_h
#error "PSTestJHDGameModeBase.generated.h already included, missing '#pragma once' in PSTestJHDGameModeBase.h"
#endif
#define PSTESTJHD_PSTestJHDGameModeBase_generated_h

#define PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_SPARSE_DATA
#define PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_RPC_WRAPPERS
#define PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPSTestJHDGameModeBase(); \
	friend struct Z_Construct_UClass_APSTestJHDGameModeBase_Statics; \
public: \
	DECLARE_CLASS(APSTestJHDGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/PSTestJHD"), NO_API) \
	DECLARE_SERIALIZER(APSTestJHDGameModeBase)


#define PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPSTestJHDGameModeBase(); \
	friend struct Z_Construct_UClass_APSTestJHDGameModeBase_Statics; \
public: \
	DECLARE_CLASS(APSTestJHDGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/PSTestJHD"), NO_API) \
	DECLARE_SERIALIZER(APSTestJHDGameModeBase)


#define PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APSTestJHDGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APSTestJHDGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APSTestJHDGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APSTestJHDGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APSTestJHDGameModeBase(APSTestJHDGameModeBase&&); \
	NO_API APSTestJHDGameModeBase(const APSTestJHDGameModeBase&); \
public:


#define PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APSTestJHDGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APSTestJHDGameModeBase(APSTestJHDGameModeBase&&); \
	NO_API APSTestJHDGameModeBase(const APSTestJHDGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APSTestJHDGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APSTestJHDGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APSTestJHDGameModeBase)


#define PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_12_PROLOG
#define PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_SPARSE_DATA \
	PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_RPC_WRAPPERS \
	PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_INCLASS \
	PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_SPARSE_DATA \
	PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PSTESTJHD_API UClass* StaticClass<class APSTestJHDGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PSTestJHD_Source_PSTestJHD_PSTestJHDGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
