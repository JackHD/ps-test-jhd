// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PSTestJHD/PSTestJHDGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePSTestJHDGameModeBase() {}
// Cross Module References
	PSTESTJHD_API UClass* Z_Construct_UClass_APSTestJHDGameModeBase_NoRegister();
	PSTESTJHD_API UClass* Z_Construct_UClass_APSTestJHDGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_PSTestJHD();
// End Cross Module References
	void APSTestJHDGameModeBase::StaticRegisterNativesAPSTestJHDGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_APSTestJHDGameModeBase_NoRegister()
	{
		return APSTestJHDGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_APSTestJHDGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APSTestJHDGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_PSTestJHD,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APSTestJHDGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "PSTestJHDGameModeBase.h" },
		{ "ModuleRelativePath", "PSTestJHDGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APSTestJHDGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APSTestJHDGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APSTestJHDGameModeBase_Statics::ClassParams = {
		&APSTestJHDGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_APSTestJHDGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APSTestJHDGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APSTestJHDGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APSTestJHDGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APSTestJHDGameModeBase, 2399064103);
	template<> PSTESTJHD_API UClass* StaticClass<APSTestJHDGameModeBase>()
	{
		return APSTestJHDGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APSTestJHDGameModeBase(Z_Construct_UClass_APSTestJHDGameModeBase, &APSTestJHDGameModeBase::StaticClass, TEXT("/Script/PSTestJHD"), TEXT("APSTestJHDGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APSTestJHDGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
