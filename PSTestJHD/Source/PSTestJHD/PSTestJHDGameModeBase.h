// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PSTestJHDGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PSTESTJHD_API APSTestJHDGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
