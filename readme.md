# Infinite Runner Prototype [PSTestJHD]
## How to play
### Goal
Avoid the blocks and collect the rings. Survive for as long as you can and get the highest score!
My high score is 100.
### Controls:
Mobile: Press the **left and right buttons** at the bottom of the screen to move, press the **middle button** to jump.
PC: Press **A or D** to move, **SPACE** to jump.
## Issues during Development
- I had not used Unreal in a number of years and was not even that familiar with it back then. For this project I had to learn a lot about how it works.

- I had issues getting the game running on the target hardware. There were a number of issues but ultimately the build did not work. I got it working in the end but with the caveat that the APK is larger than 100MB. A workaround would be needed if the game was published to the Google Play Store, as that has an APK size limit of 100 MB.

- UI did not scale properly at all to mobile resolutions. Required me to learn how Unreal’s UI system works. The UI should now support any reasonable resolution and aspect ratio.

- A mysterious untextured sphere randomly appears sometimes. This does not appear anywhere in the scene hierarchy.
## What I would do differently
### In the time I had
- All moving objects should have been children of a class that handles the movement.
- I would design the spawning system differently to allow more uniform spawns.
- The blueprints are messy and most lack comments.
### If I had more time
- The game looks horrible when built, the reflections and lighting do not work properly. This issue could probably be fixed with some messing about with graphics settings.

- Although they do not affect gameplay, the graphical issues caused by moving scenery need to be fixed. Bigger tiles may help as the artefacts seem to appear on the joins between them.

- Testing different control methods may benefit the game. For example the player could track their finger along the bottom of the screen to move the character and swipe up to jump.